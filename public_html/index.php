<?PHP

header( 'Location: ./index.html' ) ;
/*
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

$blocked_users = array (
	'caboindex',
) ;

$allowed_file_types = array ( 'jpg' , 'jpeg' , 'png' , 'gif' , 'ogg' , 'ogv' , 'oga' , 'svg' , 'xcf' , 'midi' , 'mid' , 'djvu' , 'pdf' , 'tif' , 'tiff' ) ;

require_once ( 'php/common.php' ) ;
include_once ( 'php/wikiquery.php' ) ;


function do_direct_upload ( $url , $new_name , $desc ) {
	global $perl_command , $project ;

	$comment = "Transferred from $url" ;
	
	$params = array(
		'action' => 'upload' ,
		'botmode' => 1,
		'url' => $url ,
		'newfile' => $new_name ,
		'desc' => $desc ,
		'comment' => $comment
	) ;

	print "<pre>" ; print_r ( $params ) ; print "</pre>" ;
	
	$res = do_post_request ( 'http://tools.wmflabs.org/magnustools/oauth_uploader.php' , $params ) ;
	
	print "<pre>" ; print_r ( $res ) ; print "</pre>" ;
	
	return $res['error'] ;
}

#_______________________

# Prep
$project = "wikimedia" ;
$simulate = $_REQUEST['doit'] == 'Simulate' ? 1 : 0 ;
$urls_text = get_request ( 'urls' , '' ) ;
$main_desc = get_request ( 'description' , '' ) ;

print get_common_header ( "url2commons.php" , "URL2Commons" ) ;
print "This tool can upload files from generic URLs to Wikimedia Commons.<br/>" ;
print "PLEASE only upload files that are under a free license. Thanks!<br/>" ;
print "<div><span style='color:red:font-weight:bold'>ATTENTION!</span> To use this tool, first <a href='/magnustools/oauth_uploader.php' target='_blank'>authorise</a> OAuth Uploader to upload in your name!</div>" ;

$error = 0 ;
if ( isset ( $_REQUEST['doit'] ) ) {
	$wq = new WikiQuery ( 'commons' , 'wikimedia' ) ;
	$urls = explode ( "\n" , $urls_text ) ;
	if ( $simulate ) print "<h2>Simulation - no files will be uploaded</h2>" ;
	else "<h2>Upload in progress</h2>" ;



	$had_that = array () ;
	foreach ( $urls AS $url ) {
		if ( trim ( $url ) == '' ) continue ;
		$u = explode ( ' ' , trim ( $url ) , 2 ) ;
		$url = array_shift ( $u ) ;
		$descriptor = '' ;
		$newname = array_pop ( explode ( '/' , $url ) ) ;
		if ( count ( $u ) > 0 ) {
			$u = array_shift ( $u ) ;
			$u = explode ( '|' , trim ( $u ) , 2 ) ;
			$n = array_shift ( $u ) ;
			if ( trim ( $n ) != '' ) $newname = $n ;
			if ( count ( $u ) > 0 ) {
				$descriptor = trim ( array_shift ( $u ) ) ;
			}
		}
		$desc = $main_desc ;
		$desc = str_ireplace ( '$DESCRIPTOR$' , $descriptor , $desc ) ;
		$desc = str_ireplace ( '$URL$' , $url , $desc ) ;
		
		$newname = trim ( str_replace ( '_' , ' ' , $newname ) ) ;
		$newname = ucfirst ( $newname ) ;
		
		$exists = $wq->does_image_exist ( "File:" . $newname ) ;
		if ( !$exists and isset ( $had_that[$newname] ) ) $exists = 1 ;
		$had_that[$newname] = 1 ;
		
		$good_type = in_array ( strtolower ( array_pop ( explode ( '.' , $newname ) ) ) , $allowed_file_types ) ;
		
		if ( $exists ) print "<div style='color:white;background:red'>FILE <b>$newname</b> ALREADY EXISTS OR IS USED TWICE IN THIS LIST - NOT UPLOADING!<br/>" ;
		else if ( !$good_type ) print "<div style='color:white;background:red'>FILE <b>$newname</b> DOES NOT HAVE AN ACCEPTED FILE TYPE - NOT UPLOADING!<br/>" ;
		else print "Uploading as <b>" . htmlspecialchars ( $newname ) . "</b>:<br/>" ;
		print "<pre>" . htmlspecialchars ( $desc ) . "</pre><hr/>" ;
		if ( $exists or !$good_type ) {
			print "</div>" ;
			$error++ ;
		}
		
		if ( !$simulate and !$exists and $good_type ) {
			$result = do_direct_upload ( $url , $newname , $desc ) ;
			if ( $result != 'OK' ) {
				print "Upload error, aborting: $result" ;
				exit ( 0 ) ;
			}
		}
	}
	
} else {
	$main_desc = '{{Information
| Description    = {{en|$DESCRIPTOR$}}
| Date           = 
| Source         = $URL$
| Author         = 
| Permission     = 
| Other_versions = 
}}
{{LicenseReview}}

[[Category:]]' ;
}

if ( $error > 0 ) print "$error ERRORS, SIMULATE AGAIN!</br>" ;
else if ( $simulate and trim ( $urls_text ) != '' ) print "<div style='color:white;background:green'>ALL OK, UPLOAD BUTTON AVAILABLE</div>" ;

print "<form method='post'>
<table border='1'>
<tr><th rowspan=2>URLs</th><td><textarea style='width:100%' cols=120 rows=15 name='urls'>$urls_text</textarea></td></tr>
<tr><td>Enter File URLs above, one per line. Optionally, follow the URL with a space and the new filename. Optionally, follow the new filename (or blank) with a \"|\" and a description key (see below).</td></tr>
<tr><th rowspan=2>Description</th><td><textarea style='width:100%' cols=120 rows=15 name='description'>$main_desc</textarea></td></tr>
<tr><td>Enter the {{Information}} template, including categories etc. The string \"\$DESCRIPTOR$\" will be replaced with the description key (see above) for each file, e.g., volume numbers, species name etc. \"\$URL\" will automatically be replaced with the respective source URL.</td></tr>
<tr><th /><td><input type='submit' name='doit' value='Simulate' />" ;

if ( $error == 0 and $simulate ) print " <input type='submit' name='doit' value='Upload' />" ;
 
 print "</td></tr>
</table>
</form>" ;


print get_common_footer() ;
*/
?>