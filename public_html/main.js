//var allowed_file_types = [ 'jpg' , 'jpeg' , 'png' , 'gif' , 'ogg' , 'ogv' , 'oga' , 'svg' , 'xcf' , 'midi' , 'mid' , 'djvu' , 'pdf' , 'tif' , 'tiff' ] ;
var allowed_file_types = ['png','gif','jpg','jpeg','tiff','tif','xcf','pdf','mid','ogg','ogv','svg','djvu','oga','flac','opus','wav','webm'] ;

var simulation = true ;
var sim_counter ;
var bad_counter ;
var new_filenames ;

function checkCounters() {
	if ( sim_counter > 0 ) return ;
	
	if ( bad_counter == 0 ) {
		if ( !simulation ) {
			$('#counter_result').addClass('good').html ( "All files uploaded!" ) ;
			$('#upload_button').hide() ;
			return ;
		}
		$('#counter_result').addClass('good').html ( "All simulations successful! Upload button enabled!" ) ;
		$('#upload_button').show() ;
	} else {
		$('#counter_result').addClass('bad').html ( bad_counter + " "+(simulation?'simulations':'uploads')+" failed." ) ;
		$('#upload_button').hide() ;
	}
}

function doUploadFile ( o ) {

	$('#'+o.id+' span.check').html ( "Uploading file to Commons..." ) ;

	var params = {
		action:'upload',
		newfile:o.newname,
		url:o.url,
		desc:o.desc,
		comment:'Transferred from ' + o.url,
		botmode:1
	} ;

	$.post ( '/magnustools/oauth_uploader.php?rand='+Math.random() , params , function ( d ) {
//		console.log ( d ) ;
		sim_counter-- ;
		if ( d.error == 'OK' ) {
			$('#'+o.id+' span.check').addClass('good').html ( "SUCCESS!" ) ;
			var out = "<div>Expected file on Commons: <a href='//commons.wikimedia.org/wiki/File:"+encodeURIComponent(o.newname)+"'>"+o.newname+"</a></div>" ;
			$('#'+o.id+' div.desc').append ( out ) ;
		} else {
			var h = "ERROR: " + d.error ;
			if ( undefined !== ((d.res||{}).upload||{}).warnings ) {
				var h = "Upload warning" ;
				if ( undefined !== d.res.upload.warnings.duplicate ) {
					h = "File exists on Commons as " ;
					h += "<a target='_blank' href='//commons.wikimedia.org/wiki/File:" + encodeURIComponent(d.res.upload.warnings.duplicate[0]) + "'>" ;
					h += d.res.upload.warnings.duplicate[0].replace(/_/g,' ') ;
					h += "</a>" ;
				}
			}
			$('#'+o.id+' span.check').addClass('bad').html ( h ) ;
			bad_counter++ ;
		}
		checkCounters();
	} , 'json' ).error(function(x) {
		var h = "ERROR : " + x.status + " " + x.statusText + "</b>" ;
		$('#'+o.id+' span.check').addClass('bad').html ( h ) ;
		sim_counter-- ;
		bad_counter++ ;
		checkCounters();
	} ) ;
}

function addUpload ( o ) {
	o.url = o.url.replace ( /\$US\$/g , "_" ) ;

	var h = '' ;
	h += "<div class='uploading' id='"+o.id+"'><div><b>" ;
	h += (simulation?"Simulation":"Upload") + "</b> of " ;
	h += "<a class='original' target='_blank' href='" + o.url + "'>" + o.url + "</a>" ;
	h += " <span class='check'>Checking...</span>" ;
	h += "</div><div class='desc'></div>" ;
	h += "</div>" ;
	$('#output').append ( h ) ;

	if ( undefined !== source_urls[o.url] ) {
		$('#'+o.id+' span.check').addClass('bad').html ( 'You are already trying to upload a file with this URL!' ) ;
		bad_counter++ ;
		sim_counter-- ;
		return ;
	}
	source_urls[o.url] = true ;
	
	if ( o.newname == '' ) {
		var a = o.url.split ( '/' ) ;
		o.newname = a.pop() ;
	}
	
	var type = o.newname.split ( '.' ) ;
	type = $.trim(type.pop().toLowerCase()) ;
	if ( -1 == $.inArray ( type , allowed_file_types ) ) {
		$('#'+o.id+' span.check').addClass('bad').html ( '"'+type+'" is not a supported file name!' ) ;
		bad_counter++ ;
		sim_counter-- ;
		return ;
	}

	$('#'+o.id+' a.original').text ( o.newname ) ;
	
	if ( undefined !== new_filenames[o.newname] ) {
		$('#'+o.id+' span.check').addClass('bad').html ( 'You are already trying to upload a file with this filename!' ) ;
		bad_counter++ ;
		sim_counter-- ;
		return ;
	}
	new_filenames[o.newname] = true ;

	o.desc = $('#description').val() ;
	o.desc = o.desc.replace ( /\$DESCRIPTOR\$/g , o.minidesc ) ;
	o.desc = o.desc.replace ( /\$URL\$/g , o.url ) ;
	o.desc = o.desc.replace ( /\$NL\$/g , "\n" ) ;
	o.desc = o.desc.replace ( /\$US\$/g , "_" ) ;
	o.desc = o.desc.replace ( /\[\[Category:\]\]/g , '' ) ;
	o.desc = $.trim ( o.desc ) ;
	$('#'+o.id+' div.desc').html ( "<pre>" + o.desc + "</pre>" ) ;

	
	var wp = new WikiPage ( { lang:'commons' , project:'wikimedia' , title:o.newname } ) ;
	wp.checkExists ( function () {
		$('#'+o.id+' span.check').addClass('bad').html ( "File exists!" ) ;
		bad_counter++ ;
		sim_counter-- ;
		checkCounters();
	} , function () {
		if ( !simulation ) {
			doUploadFile ( o ) ;
			return ;
		}
		$('#'+o.id+' span.check').addClass('good').html ( "Filename available." ) ;
		sim_counter-- ;
		checkCounters();
	} ) ;
	
}

function simulateUpload ( sim ) {
	new_filenames = {} ;
	source_urls = {} ;
	simulation = sim ;
	sim_counter = 0 ;
	bad_counter = 0 ;
	$('#output').html ( '' ) ;
	
	$.each ( $('#urls').val().split("\n") , function ( k , row ) {
		row = $.trim ( row ) ;
		var m = row.match ( /^(\S+)\s(.*?)\|(.*)$/ ) ;
		if ( m == null ) m = row.match ( /^(\S+)\s(.*)$/ ) ;
		if ( m == null ) m = row.match ( /^(\S+)$/ ) ;
		if ( m == null ) return ; // Blank line?
		sim_counter++ ;
		var o = { id:'url_'+k , num:k , url:m[1] , newname:(m[2]||'') , minidesc:(m[3]||'') } ;
		addUpload ( o ) ;
	} ) ;
	
	$('#output').append ( "<div class='uploading' id='counter_result'></div>" ) ;
	
	checkCounters() ;
}

function getUrlVars () {
	var vars = {} ;
	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1) ;
	var hash = window.location.href.slice(window.location.href.indexOf('#') + 1) ;
	if ( hash == window.location.href ) hash = '' ;
	if ( hash.length > 0 ) hashes = hash ;
	else hashes = hashes.replace ( /#$/ , '' ) ;
	hashes = hashes.split('&');
	$.each ( hashes , function ( i , j ) {
		var hash = j.split('=');
		hash[1] += '' ;
		vars[hash[0]] = decodeURI(hash[1]).replace(/_/g,' ');
	} ) ;
	return vars;
}


$(document).ready ( function () {
	loadMenuBarAndContent ( { toolname : 'URL2Commons' , meta : 'URL2Commons' , content : 'form.html' , run : function () {
		wikiDataCache.ensureSiteInfo ( [ { lang:'commons' , project:'wikimedia' } ] , function () {
			var params = getUrlVars() ;
			if ( typeof params.urls != 'undefined' ) $('#urls').val ( unescape(params.urls) ) ;
			if ( typeof params.desc != 'undefined' ) $('#description').val ( unescape(params.desc) ) ;
//			$('#urls').val ( "https://farm8.staticflickr.com/7292/10779384965_aee6a95828_o_d.jpg Test image 2014-03-06.jpg|Test image" ) ; // TESTING
			$('#simulate').click ( function() {simulateUpload(true)} ) ;
			$('#upload_button').click ( function() {simulateUpload(false)} ) ;
			if ( typeof params.simulate != 'undefined' ) $('#simulate').click() ;
			if ( typeof params.run != 'undefined' ) $('#upload_button').click() ;
		} ) ;
	} } )
} ) ;
